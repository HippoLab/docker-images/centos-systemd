FROM centos

## Tell systemd we are running in container
ENV container=docker

## Use system target with minimal dependencies
COPY container.target /etc/systemd/system/container.target

## Do not remove "iproute2", it is required to make {{ ansible_interfaces }} work
RUN ln -sf /etc/systemd/system/container.target /etc/systemd/system/default.target && \
    yum install -y iproute && \
    yum clean all

## Run systemd by default
ENTRYPOINT ["/sbin/init"]

CMD ["--log-level=info"]

## This makes systemd correctly respond on 'docker stop' command
STOPSIGNAL SIGRTMIN+3
